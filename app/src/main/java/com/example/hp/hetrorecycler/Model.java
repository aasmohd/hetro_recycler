package com.example.hp.hetrorecycler;

public class Model {

    public static final int MSG_TYPE_SENT=0;
    public static final int MSG_TYPE_RECEIVED=1;

    public int type;
    public String text;

    public Model(int type, String text)
    {
        this.type=type;
        this.text=text;
    }
}