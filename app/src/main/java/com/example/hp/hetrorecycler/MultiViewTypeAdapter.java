package com.example.hp.hetrorecycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MultiViewTypeAdapter extends RecyclerView.Adapter {

    private ArrayList<Model> dataSet;
    Context mContext;

    public MultiViewTypeAdapter(ArrayList<Model> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //3
        View view;
        switch (viewType) {
            case Model.MSG_TYPE_SENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_right, parent, false);
                return new SentTypeViewHolder(view);
            case Model.MSG_TYPE_RECEIVED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_left, parent, false);
                return new ReceivedTypeViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        //2

        switch (dataSet.get(position).type) {
            case 0:
                return Model.MSG_TYPE_SENT;
            case 1:
                return Model.MSG_TYPE_RECEIVED;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

       //4
        Model object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case Model.MSG_TYPE_SENT:
                    ((SentTypeViewHolder) holder).txtType.setText(object.text);

                    break;
                case Model.MSG_TYPE_RECEIVED:
                    ((ReceivedTypeViewHolder) holder).txtType.setText(object.text);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();//1
    }

    public static class SentTypeViewHolder extends RecyclerView.ViewHolder {
        TextView txtType;
        LinearLayout linearLayout;

        public SentTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
        }
    }

    public static class ReceivedTypeViewHolder extends RecyclerView.ViewHolder {
        TextView txtType;

        public ReceivedTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
        }
    }
}

